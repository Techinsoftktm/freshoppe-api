<?php

namespace App\Providers;

use App\Models\User;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

class CustomServiceProvider implements UserProvider
{
    public function retrieveByToken ($identifier, $token) {
        throw new Exception('Method not implemented.');
    }

    public function updateRememberToken (Authenticatable $user, $token) {
        throw new Exception('Method not implemented.');
    }

    public function retrieveById ($identifier) {
        return User::find($identifier);
    }

    public function retrieveByCredentials (array $credentials) {
        $mobile = $credentials['mobile'];
		$otp 	= $credentials['otp'];
        return User::where('mobile', $mobile)->where('otp', $otp)->first();
    }

    public function validateCredentials (Authenticatable $user, array $credentials) {
        return true;
    }
}