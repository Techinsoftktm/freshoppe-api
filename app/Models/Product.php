<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Freshbitsweb\LaravelCartManager\Traits\Cartable;

class Product extends Model
{
    use Cartable;
    
    /**
     * @var string
     */
    protected $table = 'products';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recipes()
    {
        return $this->hasMany(Recipe::class);
    }
    
    public function getPrice()
    {
        if($this->sale_price > 0)
            return $this->sale_price;
        else
            return $this->price;
    }
    
    public function getName()
    {
        if($this->ecom_name != '')
            return $this->ecom_name;
        else
            return $this->name;
    }
	
}
