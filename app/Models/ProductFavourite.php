<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProductFavourite extends Model
{
    /**
     * @var string
     */
    protected $table = 'product_favourites';
	
	public function user()
    {
        return $this->belongsTo(User::class);
    }
	
	public function product()
    {
        return $this->belongsTo(Product::class);
    }
	
}
