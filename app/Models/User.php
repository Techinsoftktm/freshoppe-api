<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;
//use Illuminate\Notifications\Notifiable;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable, HasFactory;

	//use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile','otp','otp_verified','google_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
	
	/**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
	public function getJWTIdentifier()
    {
        return $this->getKey();
    }
	
	 /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
	
	public function addresses()
    {
        return $this->hasMany(CustomerAddress::class)->where('status',1);
    }
	
	public function favourites()
    {
        return $this->belongsToMany(Product::class, 'product_favourites', 'user_id', 'product_id');
    }
	
	public function saveLater()
    {
        return $this->belongsToMany(Product::class, 'product_saved_items', 'user_id', 'product_id');
    }
    
    public function orders()
    {
        return $this->hasMany(Order::class)->orderBy('id','desc');
    }

}
