<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order_transaction_headers';

    protected $fillable = [
        'store_id', 'order_number', 'user_id', 'address_id', 'item_count', 'order_status', 'order_type', 'notes', 'rating', 'feedback', 'recorded_by' ,'promocode_id', 'delivery_slot_time', 'delivery_slot_date'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class,'order_transaction_headers_id', 'id');
    }
	
	public function payment()
    {
        return $this->hasOne(OrderPayment::class,'order_transaction_headers_id', 'id');
    }
	
	public function shipAddress()
    {
        return $this->hasOne(CustomerAddress::class,'id', 'address_id');
    }
}