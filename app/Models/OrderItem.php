<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_transaction_details';

    protected $fillable = [
        'order_transaction_headers_id', 'product_id', 'quantity', 'net_weight', 'gross_weight', 'price', 'gst_rate', 'sub_total'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
