<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductCategory;
use Auth;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getNewProducts($full = null)
	{
		try {
			if($full){
				$products =  Product::with('images')
				->where('status',1)
				->where('ecom_status',1)
				->where('new_arrival',1)
				->inRandomOrder()
				->get();
			}
			else{
				$products =  Product::with('images')
				->where('status',1)
				->where('ecom_status',1)
				->where('new_arrival',1)
				->inRandomOrder()
				->take(12)
				->get();
			}
			
			$response['products']	= $products;
			$response['name']		= 'New Arrivals';
			return $this->sendResponse('New products retrieved successfully', $response);

        } catch (\Exception $e) {

            return response()->json(['message' => 'No new products found!'], 404);
        }
	}
	
	public function getSpeedDeals($full = null)
	{
		try {
		    if($full){
                $products =  Product::with('images')
    			->where('status',1)
    			->where('ecom_status',1)
    			->where('speed_deals',1)
    			->inRandomOrder()
    			->get();
		    }else{
		        $products =  Product::with('images')
    			->where('status',1)
    			->where('ecom_status',1)
    			->where('speed_deals',1)
    			->inRandomOrder()
    			->take(8)
    			->get();
		    }

			$response['products']	= $products;
			$response['name']		= 'Speed Deals';
			return $this->sendResponse('Speed deals retrieved successfully', $response);

        } catch (\Exception $e) {

            return response()->json(['message' => 'No speed deals found!'], 404);
        }
	}
	
	public function getTodaysSpecials($full = null)
	{
		try {
		    if($full){
                $products =  Product::with('images')
    			->where('status',1)
    			->where('ecom_status',1)
    			->where('todays_special',1)
    			->inRandomOrder()
    			->get();
		    }else{
		        $products =  Product::with('images')
    			->where('status',1)
    			->where('ecom_status',1)
    			->where('todays_special',1)
    			->inRandomOrder()
    			->take(12)
    			->get();
		    }
			
			$response['products']	= $products;
			$response['name']		= 'Todays Specials';
			return $this->sendResponse('Todays specials retrieved successfully', $response);
			
        } catch (\Exception $e) {

            return response()->json(['message' => 'No todays specials found!'], 404);
        }
	}
	
	public function getProductDetails($slug)
	{
		try {
            $product 			=  Product::with('images')
									->with('recipes')
									->where('slug','=',$slug)
									->where('status',1)
									->where('ecom_status',1)
									->first();
			$catName            = $product->categories->pluck('name')->toArray();
			$product->category_name = $catName[0];
			$slug               = Str::slug($catName[0]);
			$product->category_slug = $slug;
			$catProducts = array();
			/* if($product->categories){
				$catIds	 			=  $product->categories->pluck('id')->toArray();
				$catProducts 		=  $this->getCatRelatedProducts($catIds,$product->id);
			} */
			$relatedProducts 		=  $this->getRelatedProducts($product->id,$product->raw_item_id);
            
			$collection 			= collect($relatedProducts);
			$related 				= $collection->slice(0,8);
			$catRelated 			= $collection->slice(8)->toArray();
			$response['product']	= $product;
			$response['related']	= $related;
			$response['catRelated']	= array_values($catRelated);
			return $this->sendResponse('Product details retrieved successfully', $response);

        } catch (\Exception $e) {

            return response()->json(['message' => 'No product found!'], 404);
        }
	}
	
	public function getRelatedProducts($productId,$productRawId)
	{
		$products = Product::with('images')
					->where('raw_item_id',$productRawId)
					->where('id','<>',$productId)
					->where('status',1)
					->where('ecom_status',1)
					->inRandomOrder()
					->get();

		return $products;
	}
	
	public function getCatRelatedProducts($categoryIds,$productId)
	{
		$productCat   = ProductCategory::where('product_id','<>',$productId)
						->whereIn('category_id',$categoryIds)
						->get();
		$productIds	= $productCat->pluck('product_id')->toArray();			 
		
		$products = Product::with('images')
					->whereIn('id',$productIds)
					->where('status',1)
					->where('ecom_status',1)
					->inRandomOrder()
					->get();

		return $products;
	}
	
	public function searchProduct(Request $request)
	{
		$search	=	 $request->search;
		/*$products = Product::where('status',1)
						->where('ecom_status',1)
						->where('name','like', '%' . $search . '%')
						->orWhere('ecom_name','like', '%' . $search . '%')
						->get();*/
$products = Product::where('status',1)
						->where('ecom_status',1)
						->where(function($query) use ($search) {
						    $query->where('name','like', '%' . $search . '%')
                            ->orWhere('ecom_name','like', '%' . $search . '%');
                        })
						->get();
		return $products;
	}
	
	public function addToFavourites(Request $request)
	{
		$user 		= Auth::user();
		$productId 	= $request->productId;
		try {
			$user->favourites()->toggle($productId);
			$favourites = $user->favourites->pluck('id');
			$response['favourites']	= $favourites;
			return $this->sendResponse('Product added to favourites', $response);
		} catch (\Exception $e) {

            return response()->json(['message' => 'Sorry Please try again later'], 409);
        }
	}
	
	public function saveForLater(Request $request)
	{
		$user 		= Auth::user();
		$productId 	= $request->productId;
		try {
			$user->saveLater()->toggle($productId);
			$saveLater = $user->saveLater->pluck('id');
			$response['saveLater']	= $saveLater;
			return $this->sendResponse('Product added to save later', $response);
		} catch (\Exception $e) {

            return response()->json(['message' => 'Sorry Please try again later'], 409);
        }
	}
	
}
