<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use  App\Models\User;
use  App\Models\Product;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function profile()
    {
	
		$user 		= Auth::user();
		$address 	= $user->addresses;
		$favourites = $user->favourites->pluck('id');
		$response['user']['id']		    = $user->id;
		$response['user']['name']		= $user->name;
		$response['user']['email']		= $user->email;
		$response['user']['mobile']		= $user->mobile;
		$response['user']['google_id']	= $user->google_id;
		$response['address']	        = $address;
		$response['favourites']	        = $favourites;
		return $this->sendResponse('Profile details retrieved successfully', $response);
    }

    /**
     * Get all User.
     *
     * @return Response
     */
    public function allUsers()
    {
        return response()->json(['users' =>  User::all()], 200);
    }

    /**
     * Get one user.
     *
     * @return Response
     */
    public function singleUser($id)
    {
        try {
            $user = User::findOrFail($id);

            return response()->json(['user' => $user], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }
	
	public function saveProfile(Request $request)
    {
		$user 		= Auth::user();
		$this->validate($request, [
			'name' => ['required'],
			'email' => ['required','email','unique:users,email,' . $user->id],
			'mobile' => ['required', 'digits:10','unique:users,mobile,' . $user->id],
		]);
		$user 		= Auth::user();
		$user->name 	= $request->name;
		$user->email 	= $request->email;
		$user->mobile 	= $request->mobile;
		$user->save();
		$token = auth()->login($user);
		$data = [
			'token' => $token,
			'token_type' => 'bearer',
		];
		$user = Auth::user()->toArray();
		$data = array_merge($data, $user);
		$response['user']		= $data;
		return $this->sendResponse('Profile details saved successfully', $response);
    }
	
	public function getFavouriteList()
	{
		try {
			$user 		= Auth::user();
			$productIds = $user->favourites->pluck('id')->toArray();
			$response 	= array();
			if(count($productIds) > 0){
				$products = Product::with('images')
						->whereIn('id',$productIds)
						->where('status',1)
						->where('ecom_status',1)
						->get();
				$response['favourites']	= $products;
			}
			return $this->sendResponse('Favourites retrieved successfully', $response);

        } catch (\Exception $e) {

            return response()->json(['message' => 'No favourites found!'], 404);
        }
	}
	
	public function getSavedItemsList()
	{
		try {
			$user 		= Auth::user();
			$productIds = $user->saveLater->pluck('id')->toArray();
			$response 	= array();
			if(count($productIds) > 0){
				$products = Product::with('images')
						->whereIn('id',$productIds)
						->where('status',1)
						->where('ecom_status',1)
						->get();
				$response['savedItems']	= $products;
			}
			return $this->sendResponse('Saved items retrieved successfully', $response);

        } catch (\Exception $e) {

            return response()->json(['message' => 'No saved items found!'], 404);
        }
	}
	
	public function getPastOrders()
	{
	    try {
			$user 		= Auth::user();
			$orders     = $user->orders()->with('items')->with('items.product')->with('payment')->with('payment.paymentType')->with('shipAddress')->get();
			$response 	= array();
			$response['orders']	= $orders;
			return $this->sendResponse('Orders retrieved successfully', $response);

        } catch (\Exception $e) {

            return response()->json(['message' => 'No orders found!'], 404);
        }
	}
	
	
}
