<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }
    
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
		$this->validate($request, [
			'mobile' => ['required', 'digits:10'],
		]);
		try {
			$user = User::where('mobile',$request->mobile)->first();
			
			// Account details
			$rndno 	= rand(100000, 999999);
			/* $apiKey = urlencode('3TAUrc7GdT0-Zrr9y2SxpgJJ3WE2mG5pXhj7aUY29o');

			// Message details
			$sender  = urlencode('BOOKIT');
			$message = rawurlencode("Hi, Your OTP is $rndno");
			$numbers = $request->mobile;

			// Prepare data for POST request
			$datam = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

			// Send the POST request with cURL
			$ch = curl_init('https://api.textlocal.in/send/');
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $datam);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$data = curl_exec($ch);
			curl_close($ch);
			
			$responds = json_decode($data); */
			//if($responds != null){
				//if($responds->status =='success'){
					if(empty($user)){
						$input['name'] 			= 'New User';
						$input['email'] 		= $request->mobile.'@guest.in';
						$input['password'] 		= app('hash')->make('password');
						$input['mobile'] 		= $request->mobile;
						$user 					= User::create($input);
						$new_user				= true;
					}else{
						$new_user				= false;
					}
					$user->otp = $rndno;
					$user->otp_verified = 0;
					$user->save();
					$user->new_user = $new_user;
					return response()->json(['user' => $user, 'message' => 'An OTP has been sent to your Mobile Number'], 201);
				/* }else{
					return response()->json(['message' => 'Sorry Please try again later'], 401);
				} */
			/* }
			else{
				return response()->json(['message' => 'Sorry Please try again later'], 401);
			} */
		}catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }
    }

    /**
     * send otp
     *
     * @param  Request  $request
     * @return Response
     */
    public function sendOTP(Request $request)
    {
		$this->validate($request, [
			'mobile' => ['required', 'digits:10'],
		]);
		try {
			$user = User::where('mobile',$request->mobile)->first();
			
			// Account details
			$rndno 	= rand(100000, 999999);
			
			$apiKey = urlencode('xwZtarK8mIc-m418763gdkhjl6hH6ZqfB3DSTjHGSX');

			// Message details
			$sender  = urlencode('FRSHPE');
			$message = rawurlencode("Dear customer, Your OTP for login to Freshoppe is $rndno. Valid for 60 Seconds. Regards, UNIFRESH Foods.");
			$numbers = $request->mobile;

			// Prepare data for POST request
			$datam = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

			// Send the POST request with cURL
			$ch = curl_init('https://api.textlocal.in/send/');
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $datam);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$data = curl_exec($ch);
			curl_close($ch);
			
			$responds = json_decode($data);
			if($responds != null){
				if($responds->status =='success'){
					if(empty($user)){
						$input['name'] 			= 'New User';
						$input['email'] 		= $request->mobile.'@guest.in';
						$input['password'] 		= app('hash')->make('password');
						$input['mobile'] 		= $request->mobile;
						$user 					= User::create($input);
						$new_user				= true;
					}else{
						$new_user				= false;
					}
					$user->otp = $rndno;
					$user->otp_verified = 0;
					$user->save();
					$user->new_user = $new_user;
					return response()->json(['message' => 'An OTP has been sent to your Mobile Number'], 200);
				}else{
					return response()->json(['message' => 'Sorry Please try again later'], 409);
				}
			}
			else{
				return response()->json(['message' => 'Sorry Please try again later'], 409);
			}
		}catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }
    }
    
    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function loginWithOtp(Request $request)
    {
        //echo "===========";die;
        //validate incoming request 
        $this->validate($request, [
            'mobile' => ['required', 'digits:10'],
			'otp' => ['required'],
        ]);
		try {
			$user = User::where('mobile',$request->mobile)->first();
			if($user->otp == $request->otp){
				$user->otp_verified = 1;
				$user->save();
			
				$credentials = $request->only(['mobile','otp']);

				if (! $token = Auth::attempt($credentials)) {
					return response()->json(['message' => 'Unauthorized'], 401);
				}
				$data = [
					'token' => $token,
					'token_type' => 'bearer',
					//'expires_in' => Auth::factory()->getTTL() * 60
				];
				$user = Auth::user()->toArray();
				$data = array_merge($data, $user);
				return response()->json($data, 200);
			}else{
				return response()->json(['message' => 'Please enter a valid otp'], 409);
			}
		}catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Login Failed!'], 409);
        }
	}
	
	/**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    /*public function loginWithPassword(Request $request)
    {
        //validate incoming request 
        $this->validate($request, [
            'mobile' => ['required', 'digits:10'],
			'password' => ['required'],
        ]);
		try {
			$user = User::where('mobile', $request->mobile)->first();

			if (!$user) {
				return response()->json(['message' => 'Unauthorized'], 401);
			}

			// Verify the password and generate the token
			if (Hash::check($user->password,$request->password)) {
				$token = auth()->login($user);
				$data = [
					'token' => $token,
					'token_type' => 'bearer',
					'expires_in' => Auth::factory()->getTTL() * 60
				];
				$user = Auth::user()->toArray();
				$data = array_merge($data, $user);
				return response()->json($data, 200);
			}
			
			// Bad Request response
			return response()->json([
				'message' => 'Mobile or password is wrong.'
			], 400);
			
		}catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Login Failed!'], 409);
        }
	}*/
	
	public function loginWithPassword(Request $request)
    {
        //validate incoming request 
        $this->validate($request, [
            //'mobile' => ['required', 'digits:10'],
			'email' => ['required','email'],
			'password' => ['required'],
        ]);
		try {
			$user = User::where('email', $request->email)->first();

			if (!$user) {
				return response()->json(['message' => 'Invalid email address'], 401);
			}

			// Verify the password and generate the token
			if (Hash::check($request->password,$user->password)) {
				$token = auth()->login($user);
				$data = [
					'token' => $token,
					'token_type' => 'bearer',
					//'expires_in' => Auth::factory()->getTTL() * 60
				];
				$user = Auth::user()->toArray();
				$data = array_merge($data, $user);
				return response()->json($data, 200);
			}
			
			// Bad Request response
			return response()->json([
				'message' => 'Email or password is wrong.'
			], 401);
			
		}catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Login Failed!'], 409);
        }
	}
	
	/**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function googleSignIn(Request $request)
    {
        $this->validate($request, [
            'loginType' => ['required'],
			'google_id' => ['required'],
			'email' => ['required'],
			'name' => ['required'],
        ]);
		try {

			$user = User::where('google_id', $request->google_id)
            ->orWhere('email', $request->email)
            ->first();
            
			if(empty($user)){
				$user = User::create([
					'name' => $request->name,
					'email' => $request->email,
					'google_id' => $request->google_id,
					'password' => app('hash')->make('password')
				]);
			} else {
				// Update the token on each login request
				$user['google_id'] = $request->google_id;
				$user->save();
			}
			$token = auth()->login($user);
			$data = [
				'token' => $token,
				'token_type' => 'bearer',
				//'expires_in' => Auth::factory()->getTTL() * 60
			];
			$user = Auth::user()->toArray();
			$data = array_merge($data, $user);
			return response()->json($data, 200);
			
		}catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Login Failed!'], 409);
        }
	}
	
	public function verifyOTP(Request $request)
	{
		$this->validate($request, [
            'mobile' => ['required', 'digits:10'],
			'otp' => ['required', 'digits:6'],
        ]);
		
		try {
			$user = Auth::user();
			if($user->otp == $request->otp){
				$user->otp_verified = 1;
				$user->mobile = $request->mobile;
				$user->save();
				$token = auth()->login($user);
				$data = [
					'token' => $token,
					'token_type' => 'bearer',
					//'expires_in' => Auth::factory()->getTTL() * 60
				];
				$user = Auth::user()->toArray();
				$data = array_merge($data, $user);
				return response()->json($data, 200);
			}else{
				return response()->json(['message' => 'Please enter a valid otp'], 409);
			}
		}catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'OTP verification failed!'], 409);
        }
	
	}
	
	/**
     * phone verification
     *
     * @param  Request  $request
     * @return Response
     */
    public function verifyPhone(Request $request)
    {
		$this->validate($request, [
			'mobile' => ['required', 'digits:10'],
		]);
		try {
			$user = Auth::user();
			$chkMobileExist = User::where('mobile',$request->mobile)->where('id','<>',$user->id)->first();
			if(!empty($chkMobileExist)){
				return response()->json(['message' => 'This mobile number is associated with another user.'], 409);
			}else{
				// Account details
				$rndno 	= rand(100000, 999999);
			
    			$apiKey = urlencode('xwZtarK8mIc-m418763gdkhjl6hH6ZqfB3DSTjHGSX');
    
    			// Message details
    			$sender  = urlencode('FRSHPE');
    			$message = rawurlencode("Dear customer, Your OTP for login to Freshoppe is $rndno. Valid for 60 Seconds. Regards, UNIFRESH Foods.");
				$numbers = $request->mobile;

				// Prepare data for POST request
				$datam = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

				// Send the POST request with cURL
				$ch = curl_init('https://api.textlocal.in/send/');
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $datam);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$data = curl_exec($ch);
				curl_close($ch);
				
				$responds = json_decode($data);
				if($responds != null){
					if($responds->status =='success'){
						//$user->mobile = $request->mobile;
						$user->otp = $rndno;
						$user->otp_verified = 0;
						$user->save();
						return response()->json(['message' => 'An OTP has been sent to your Mobile Number'], 200);
					}else{
						return response()->json(['message' => 'Sorry Please try again later'], 409);
					}
				}
				else{
					return response()->json(['message' => 'Sorry Please try again later'], 409);
				}
			}
		}catch (\Exception $e) {
            return response()->json(['message' => 'Phone number verification failed!'], 409);
        }
    }
}
