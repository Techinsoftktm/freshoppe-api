<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
    
	public function addToCart(Request $request)
    {
        try {
            $user = Auth::user();
            cart()->setUser($user->id);
    		Product::addToCart($request->productId);
            $cartItems  = $this->getCartItems();
			$response['cart']	= $cartItems;
			return $this->sendResponse('Cart retrieved successfully', $response);
        }catch (\Exception $e) {

            return response()->json(['message' => 'Something went wrong!'], 404);
        }
    }
    
    public function storeCartData(Request $request)
    {
        try {
            $user       = Auth::user();
            cart()->setUser($user->id);
    		$cartData   = $request->cartItems;
    		$cartItems  = $this->getCartItems();
    		if(!empty($cartData) && empty($cartItems)){
    		    foreach($cartData as $key=>$val){
    		        Product::addToCart($val['id'],$val['quantity']);
    		    }
    		}
    		$cartItems  = $this->getCartItems();
            $response['cart']	= $cartItems;
			return $this->sendResponse('Cart retrieved successfully', $response);
        }catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong!'], 404);
        }
    }
    
    public function removeFromCart(Request $request)
    {
        try {
            $user = Auth::user();
            cart()->setUser($user->id);
            $cartItems  = $this->getCartItems();
            $key        = array_search($request->productId, array_column($cartItems, 'id'));
            cart()->removeAt($key);
            $cartItems  = $this->getCartItems();
            $response['cart']	= $cartItems;
			return $this->sendResponse('Cart retrieved successfully', $response);
        }catch (\Exception $e) {

            return response()->json(['message' => 'Something went wrong!'], 404);
        }
    }
    
    public function incrementCartItem(Request $request)
    {
        try {
            $user = Auth::user();
            cart()->setUser($user->id);
            $cartItems  = $this->getCartItems();
            $key        = array_search($request->productId, array_column($cartItems, 'id'));
            if($key > 0){
                cart()->incrementQuantityAt($key);
            }else{
                Product::addToCart($request->productId);
            }
            $cartItems  = $this->getCartItems();
            $response['cart']	= $cartItems;
			return $this->sendResponse('Cart retrieved successfully', $response);
        }catch (\Exception $e) {

            return response()->json(['message' => 'Something went wrong!'], 404);
        }
    }
    
    public function getCartItems()
    {
        $user = Auth::user();
        cart()->setUser($user->id);
		$cart = cart()->toArray();
		$cartItems = array();
		if (!empty($cart['items'])){
			foreach($cart['items'] as $key=>$item){
				$product = Product::find($item['modelId']);
				$cartItems[$key]['id'] 	        = $item['modelId'];
				$cartItems[$key]['quantity'] 	= $item['quantity'];
				$cartItems[$key]['name'] 	    = $item['name'];
				$cartItems[$key]['price'] 	    = $item['price'];
				$cartItems[$key]['netWt']   	= $product->net_weight;
				$cartItems[$key]['grossWt']   	= $product->gross_weight;
				$cartItems[$key]['unit']   	    = $product->unit_id;
				$cartItems[$key]['pieces']   	= $product->pieces;
			}
		}
		return $cartItems;
    }
    
    public function getCartData()
    {
        try {
            $user = Auth::user();
            cart()->setUser($user->id);
    		$cart = cart()->toArray();
    		$cartItems = array();
    		if (!empty($cart['items'])){
    			foreach($cart['items'] as $key=>$item){
    				$product = Product::find($item['modelId']);
    				$cartItems[$key]['id'] 	        = $item['modelId'];
    				$cartItems[$key]['quantity'] 	= $item['quantity'];
    				$cartItems[$key]['name'] 	    = $item['name'];
    				$cartItems[$key]['price'] 	    = $item['price'];
    				$cartItems[$key]['netWt']   	= $product->net_weight;
    				$cartItems[$key]['grossWt']   	= $product->gross_weight;
    				$cartItems[$key]['unit']   	    = $product->unit_id;
    				$cartItems[$key]['pieces']   	= $product->pieces;
    			}
    		}
		    $response['cart']	= $cartItems;
			return $this->sendResponse('Cart retrieved successfully', $response);
        }catch (\Exception $e) {

            return response()->json(['message' => 'Something went wrong!'], 404);
        }
    }
    
    public function decrementCartItem(Request $request)
    {
        try {
            $user = Auth::user();
            cart()->setUser($user->id);
            $cartItems  = $this->getCartItems();
            $key        = array_search($request->productId, array_column($cartItems, 'id'));
            cart()->decrementQuantityAt($key);
            $cartItems  = $this->getCartItems();
            $response['cart']	= $cartItems;
			return $this->sendResponse('Cart retrieved successfully', $response);
        }catch (\Exception $e) {

            return response()->json(['message' => 'Something went wrong!'], 404);
        }
    }
	
	public function clearCartData(Request $request)
    {
        //try {
           /*  $user = Auth::user();
            cart()->setUser($user->id); */
            cart()->clear();
            return response()->json(['message' => 'Cart cleard'], 200);
        /* }catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong!'], 404);
        } */
    }
    
    public function reOrder($id)
	{
	    try {
			$user 		= Auth::user();
			cart()->setUser($user->id);
			$order      = Order::find($id);
    		$cartData   = $order->items;
    		$cartItems  = $this->getCartItems();
    		if(!empty($cartData)){
    		    foreach($cartData as $key=>$val){
    		        
    		        $key        = array_search($val->product_id, array_column($cartItems, 'id'));
                    if($key > 0){
                        cart()->incrementQuantityAt($key);
                    }else{
                        Product::addToCart($val->product_id,$val->quantity);
                    }
    		    }
    		}
    		$cartItems  = $this->getCartItems();
            $response['cart']	= $cartItems;
			return $this->sendResponse('Cart retrieved successfully', $response);
        }catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong!'], 404);
        }
    }
}
