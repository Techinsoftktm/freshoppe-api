<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\CustomerAddress;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Instantiate a new AddressController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Create new address
     *
     * @return Response
     */
    public function addAddress(Request $request)
    {
		$this->validate($request, [
            'address' 		=> 'required',
			'name' 			=> 'required',
			'mobile' 		=> 'required|digits:10',
			'flat' 			=> 'required',
			'area' 			=> 'required',
			'landmark' 		=> 'required',
			'address_type' 	=> 'required'
        ]);
        try {
            $user 		= Auth::user();
			$address 	= CustomerAddress::create([
							'user_id' => $user->id,
							'address' => $request->address,
							'name' => $request->name,
							'mobile' => $request->mobile,
							'flat' => $request->flat,
							'area' => $request->area,
							'landmark' => $request->landmark,
							'address_type' => $request->address_type
						]);
			return response()->json($address, 200);
		}catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong!'], 404);
        }
    }

	/**
     * Edit address
     *
     * @return Response
     */
    public function editAddress($id, Request $request)
    {
		$this->validate($request, [
            'address' 		=> 'required',
			'name' 			=> 'required',
			'mobile' 		=> 'required|digits:10',
			'flat' 			=> 'required',
			'area' 			=> 'required',
			'landmark' 		=> 'required',
			'address_type' 	=> 'required'
        ]);
        try {
            $user 		= Auth::user();
			$address	= CustomerAddress::find($id);
			if($address){
				$address->address = $request->address;
				$address->name = $request->name;
				$address->mobile = $request->mobile;
				$address->flat = $request->flat;
				$address->area = $request->area;
				$address->landmark = $request->landmark;
				$address->address_type = $request->address_type;
				$address->save();
			}
			return response()->json($address, 200);
		}catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong!'], 404);
        }
    }
    
    /**
     * Delete address
     *
     * @return Response
     */
    public function deleteAddress($id)
    {
        try {
            $user 		= Auth::user();
			$address	= CustomerAddress::find($id);
			if($address){
				$address->status = 0;
				$address->save();
			}
			$addresses = $user->addresses;
			return response()->json($addresses, 200);
		}catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong!'], 404);
        }
    }
    
}
