<?php

namespace App\Http\Controllers;

use  App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAllCategories()
	{
		try {
            $categories =  Category::where('parent_id','=',1)
			->where('menu','=',1)
			->where('status','=',1)
			->orderBy('display_order','asc')
			->get();
			return $this->sendResponse('Categories retrieved successfully', $categories);
			
        } catch (\Exception $e) {

            return response()->json(['message' => 'No categories found!'], 404);
        }
	}
	
	public function getCategoryProducts($slug)
	{
		try {
            $category 	=  Category::where('slug','=',$slug)->first();
            $products   =  array();
            if($category){
                $products 	=  $category->products()->with('images')->where('products.status', 1)
    							->where('products.ecom_status', 1)
    							->inRandomOrder()
    							->get();
            }
			$response['category']	= $category;
			$response['products']	= $products;
			return $this->sendResponse('Products retrieved successfully', $response);
        } catch (\Exception $e) {

            return response()->json(['message' => 'No products found!'], 404);
        }
	}
}
