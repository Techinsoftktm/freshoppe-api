<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderPayment;
use App\Models\Promocode;

class OrderController extends Controller
{
    /**
     * Instantiate a new OrderController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createOrder(Request $request)
    {
        $this->validate($request, [
			'addressId' => ['required'],
			'selectedSlot' => ['required'],
			'slotDay' => ['required'],
			'paymentType' => ['required'],
			'grandTotal' => ['required'],
			'grandTotal' => ['required'],
			'subTotal' => ['required'],
		]);
        try {
            $user = Auth::user();
            cart()->setUser($user->id);
            
            $latest = Order::latest()->first();
			
			if(!empty($latest)){
    			$last_digits    = str_replace("ORD001","",$latest->order_number);
    			$order_number = (str_pad((int)$last_digits + 1, 6, '0', STR_PAD_LEFT));
    		}else{
    			$order_number = '000001';
    		}
			
			if($request->slotDay == 1)
			    $slotDate   =   date('Y-m-d');
			else
			    $slotDate   =   date('Y-m-d', strtotime("+1 day"));
    		$order = Order::create([
                'order_number'      =>  'ORD001'.$order_number,
                'user_id'           =>  $user->id,
    			'store_id'          =>  6,
                'address_id'        =>  $request->addressId,
                'item_count'        =>  0,
                'order_status'      =>  1,
                'order_type'        =>  1,
                'recorded_by'       =>  1,
                'promocode_id'      =>  $request->promocodeId,
                'delivery_slot_time'=>  $request->selectedSlot,
                'delivery_slot_date'=>  $slotDate
    		]);	
    
            if ($order) {
                $items = cart()->items();
                foreach ($items as $key=>$item){
    				$product = Product::where('id', $item['modelId'])->first();
    				$orderItem 	= new OrderItem([
    					'order_transaction_headers_id'  =>  $order->id,
    					'product_id'   					=>  $product->id,
    					'quantity'      				=>  $item['quantity'],
    					'price'         				=>  $item['price'],
    					'gst_rate'         				=>  $product->gst_rate,
    					'sub_total'         			=>  $item['price']*$item['quantity'],
    					'gross_weight'         			=>  $product->gross_weight,
    					'net_weight'         			=>  $product->net_weight,
    				]);
    				$order->items()->save($orderItem);
    			
                }
    			if($request->paymentType == 1){
    				$payment_status	= 0;
    				$payment_type_id = 5;
    			}
    			else if($request->paymentType == 2){
    				$payment_status	= 1;
    				$payment_type_id = 6;
    			}else{
    			    $payment_status	= 0;
    				$payment_type_id = 5;
    			}
    			$orderPayment 	= new OrderPayment([
    				'order_transaction_headers_id'  =>  $order->id,
    				'sub_total'    			=>  $request->subTotal,
    				'gst_rate'    			=>  $request->taxRate,
    				'gst_total'    			=>  $request->taxTotal,
    				'discount_rate'      	=>  $request->discountRate,
    				'discount_total'      	=>  $request->discountTotal,
    				'grand_total'        	=>  $request->grandTotal,
    				'payment_status'        =>  $payment_status,
    				'payment_type_id'       =>  $payment_type_id,
    				'razorpay_payment_id'   =>  $request->razorpay_payment_id,
    				'razorpay_order_id'     =>  $request->razorpay_order_id,
    				'razorpay_signature'    =>  $request->razorpay_signature,
    			]);
    			$order->payment()->save($orderPayment);
    			
    			$order->item_count = count($order->items);
    			$order->save();
    			cart()->clear();
    			$orderDetails = Order::with('items')->with('items.product')->with('payment')->with('payment.paymentType')->with('shipAddress')->find($order->id);
    			return $this->sendResponse('Order placed successfully', $orderDetails);
            }

        } catch (\Exception $e) {

            return response()->json(['message' => 'Please try again later!'], 404);
        }
    }
    
    public function cancelOrder($id)
    {
        try {
            $user   = Auth::user();
            $order  = Order::where('user_id',$user->id)->find($id);
            if($order && $order->order_status <= 2){
                $order->order_status = 7;
                $order->save();
                return $this->sendResponse('Order cancelled successfully', $order);
            }else{
                return response()->json(['message' => "You don't have access to delete this order!"], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Please try again later!'], 404);
        }
    }
    
    public function addFeedBack(Request $request, $id)
    {
        try {
            $user   = Auth::user();
            $order  = Order::where('user_id',$user->id)->find($id);
            if($order && $order->order_status == 5){
                $order->rating = $request->rating;
                $order->feedback = $request->feedback;
                $order->save();
                return $this->sendResponse('Feedback saved successfully', $order);
            }else{
                return response()->json(['message' => "You can't add feedback to this order!"], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Please try again later!'], 404);
        }
    }
    
    public function applyPromo(Request $request)
    {
        try {
           $promo = Promocode::where('code',$request->promocode)->where('status',1)->first();
           if(!$promo){
               return response()->json(['message' => "Please enter a valid promocode!"], 409);
           }else{
                if($request->total <= $promo->min_purchase_amount){
                    return response()->json(['message' => "The minimum purchase amount to avail this offer is ".$promo->min_purchase_amount], 409);
                    
                }else{
                    if($promo->offer_type == 2){
                        $discountedAmount = $promo->offer_amount;
                    }else{
                        $discountedAmount = ($request->total*$promo->offer_percentage)/100;
                    }
                    $promo->discountedAmount    =   $discountedAmount;
                    return $this->sendResponse('Promocode applied successfully', $promo);
                }
           }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Please try again later!'], 404);
        }
    }
    
    public function getOrder($number)
    {
        try {
            $order = Order::with('items')->with('items.product')->with('payment')->with('payment.paymentType')->with('shipAddress')->where('order_number',$number)->first();
    	    return $this->sendResponse('Order placed successfully', $order);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Please try again later!'], 404);
        }
    }
	
}
