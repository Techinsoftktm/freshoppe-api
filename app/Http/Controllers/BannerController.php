<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;

class BannerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getBanners()
	{
		try {
            $banners =  Banner::where('status','=',1)
			->inRandomOrder()->get();
			return $this->sendResponse('Banners retrieved successfully', $banners);

        } catch (\Exception $e) {

            return response()->json(['message' => 'No banners found!'], 404);
        }
	}
	
}
