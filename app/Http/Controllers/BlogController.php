<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getBlogs()
	{
		try {
            $blogs =  Blog::where('status','=',1)
			->inRandomOrder()->get();
			return $this->sendResponse('Blogs retrieved successfully', $blogs);

        } catch (\Exception $e) {

            return response()->json(['message' => 'No blogs found!'], 404);
        }
	}
	
}
