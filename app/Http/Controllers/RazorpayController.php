<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Razorpay\Api\Api;

class RazorpayController extends Controller
{
    /**
     * Instantiate a new RazorpayController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createOrder(Request $request)
    {
        try {
            $api 	= new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
			$order 	= $api->order->create(array('amount' => $request->amount * 100, 'currency' => 'INR'));
			return response()->json($order, 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Please try again later!'], 404);
        }
    }
    
    
	
}
