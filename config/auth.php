<?php

return [
    'defaults' => [
        'guard' => 'api',
        'passwords' => 'users',
    ],

    'guards' => [
        'api' => [
            'driver' => 'jwt',
            'provider' => 'otp-user',//users
        ],
		'web' => [
            'driver' => 'jwt',
            'provider' => 'users',//users
        ],
    ],

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => \App\Models\User::class
        ],
		'otp-user' => [
            'driver' => 'otp-based-auth-provider'
        ],
    ]
];