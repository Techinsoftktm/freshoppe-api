<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


// API route group
$router->group(['prefix' => ''], function () use ($router) {
	$router->post('register', 'AuthController@register');
	$router->post('send-otp', 'AuthController@sendOTP');
	$router->post('login-with-otp', 'AuthController@loginWithOtp');
	$router->post('login-with-password', 'AuthController@loginWithPassword');
	$router->post('google-signin', 'AuthController@googleSignIn');
    $router->get('users/profile', 'UserController@profile');
    $router->post('users/profile/save', 'UserController@saveProfile');
	$router->get('users/favourites', 'UserController@getFavouriteList');
	$router->get('users/saved-items', 'UserController@getSavedItemsList');
	$router->get('users/orders', 'UserController@getPastOrders');
    $router->get('users/{id}', 'UserController@singleUser');
    $router->get('users', 'UserController@allUsers');
	$router->get('categories', 'CategoryController@getAllCategories');
	$router->get('category/{slug}', 'CategoryController@getCategoryProducts');
	$router->get('products/new-arrivals[/{full}]', 'ProductController@getNewProducts');
	$router->get('products/todays-specials[/{full}]', 'ProductController@getTodaysSpecials');
	$router->get('products/speed-deals[/{full}]', 'ProductController@getSpeedDeals');
	$router->post('product/favourite', 'ProductController@addToFavourites');
	$router->post('product/save-for-later', 'ProductController@saveForLater');
	
	$router->get('product/{slug}', 'ProductController@getProductDetails');
	$router->get('search', 'ProductController@searchProduct');
	$router->get('banners', 'BannerController@getBanners');
	$router->get('blogs', 'BlogController@getBlogs');
	$router->post('verify-phone', 'AuthController@verifyPhone');
	$router->post('verify-otp', 'AuthController@verifyOTP');
	
	$router->get('order/{number}', 'OrderController@getOrder');
	$router->post('reorder/{id}', 'CartController@reOrder');
	$router->post('cancel-order/{id}', 'OrderController@cancelOrder');
	$router->post('add-feedback/{id}', 'OrderController@addFeedBack');
	$router->post('create-order', 'OrderController@createOrder');
	$router->post('apply-promo', 'OrderController@applyPromo');
	$router->post('create-razorpay-order', 'RazorpayController@createOrder');
	
});


$router->group(['prefix' => ''], function () use ($router) {
    $router->post('add-to-cart', 'CartController@addToCart');
    $router->post('remove-from-cart', 'CartController@removeFromCart');
    $router->post('increment-item-qty', 'CartController@incrementCartItem');
    $router->post('decrement-item-qty', 'CartController@decrementCartItem');
    $router->post('store-cart-data', 'CartController@storeCartData');
    $router->get('get-cart-data', 'CartController@getCartData');
    $router->post('clear-cart', 'CartController@clearCartData');
});

$router->group(['prefix' => 'address'], function () use ($router) {
    $router->post('new', 'AddressController@addAddress');
    $router->post('edit/{id}', 'AddressController@editAddress');
    $router->post('delete/{id}', 'AddressController@deleteAddress');
});